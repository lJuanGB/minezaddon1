package com.gmail.lJuanGBMinecraft;

import org.apache.commons.lang3.text.WordUtils;
import org.bukkit.ChatColor;
import org.bukkit.GameMode;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.PotionMeta;
import org.bukkit.potion.PotionEffect;

/**
 * Listens to when a player rightclicks with a "server potion" and applies its effects to everybody.
 * 
 * @author lJuanGB
 */
public class ServerPotionListener implements Listener{
	
	private AddonStartUp plugin;

	public ServerPotionListener(AddonStartUp plugin) {
		this.plugin = plugin;
	}
	
	@EventHandler
	public void onPlayerRightClickPotion(PlayerInteractEvent e) {
		//Check that its a right click on a serverwide potion
		if(!(e.getAction().equals(Action.RIGHT_CLICK_AIR) || e.getAction().equals(Action.RIGHT_CLICK_BLOCK))) return;
		if(e.getItem() == null) return;
		
		ItemStack potion = e.getItem();
		
		if(!potion.hasItemMeta()) return;
		if(!potion.getType().equals(Material.LINGERING_POTION)) return;
		
		PotionMeta meta = (PotionMeta) potion.getItemMeta();
		
		if(!meta.hasDisplayName()) return;
		if(!meta.getDisplayName().contains("Serverwide Potion")) return;
		
		
		e.setCancelled(true);
		
		if(!e.getPlayer().hasPermission("minez.addon1.serverpotion")) {
			e.getPlayer().sendMessage(ChatColor.RED + "You can't use Serverwide Potions!");
			return;
		}
		
		
		potion.setAmount(potion.getAmount()-1);
		e.getPlayer().getInventory().setItemInMainHand(potion);
		
		for(Player p : plugin.getServer().getOnlinePlayers()) {
			//skip player if they have the permission or they are in a non-survival mode. Send message to affected players.
			if(p.hasPermission("minez.addon1.avoidserverpotion")) continue;
			if(p.getGameMode().equals(GameMode.CREATIVE)) continue;
			if(p.getGameMode().equals(GameMode.SPECTATOR)) continue;
			
			p.addPotionEffects(meta.getCustomEffects());
			for(PotionEffect eff : meta.getCustomEffects()) {
				String effectName = WordUtils.capitalizeFully(eff.getType().getName().replaceAll("_", " "));
				p.sendMessage(ChatColor.GOLD + "You have received the effect " + ChatColor.GREEN + "" + effectName + "" + 
				ChatColor.GOLD + " courtesy of " + ChatColor.WHITE + "" + e.getPlayer().getName());
			}
		}
	}

}
