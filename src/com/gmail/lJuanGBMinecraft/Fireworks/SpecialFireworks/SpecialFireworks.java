package com.gmail.lJuanGBMinecraft.Fireworks.SpecialFireworks;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;

import org.bukkit.Color;
import org.bukkit.FireworkEffect;
import org.bukkit.Material;
import org.bukkit.FireworkEffect.Type;

import com.gmail.lJuanGBMinecraft.Fireworks.UnlockMethod;

import org.bukkit.Particle;
import org.bukkit.inventory.ItemStack;

/**
 * Here special fireworks are registered and stored. MUST be registered in uppercase.
 * 
 * @author lJuanGB
 */
public class SpecialFireworks {

	private static HashMap<String, SpecialFirework> fireworks = new HashMap<String,SpecialFirework>();
	private static List<String> fireworksIdOrdered = new ArrayList<String>();
	
	
	public SpecialFireworks() {
		
		register("SILVER", new SpecialFirework(Material.IRON_BLOCK, UnlockMethod.SILVER, Type.BALL_LARGE, false, false, Color.fromRGB(192, 192, 192), Color.fromRGB(255, 255, 255), null, Material.IRON_BLOCK));
		register("GOLD", new SpecialFirework(Material.GOLD_BLOCK, UnlockMethod.GOLD, Type.BALL_LARGE, false, true, Color.fromRGB(255, 215, 0), Color.fromRGB(255, 255, 255), null, Material.GOLD_BLOCK));
		register("PLATINUM", new SpecialFirework(Material.DIAMOND_BLOCK, UnlockMethod.PLATINUM, Type.BALL_LARGE, false, true, Color.fromRGB(0, 255, 255), Color.fromRGB(255, 255, 255), null, Material.DIAMOND_BLOCK));
		register("EMERALD", new SpecialFirework(Material.EMERALD_BLOCK, UnlockMethod.EMERALD, Type.BALL_LARGE, true, true, Color.fromRGB(50, 205, 50), Color.fromRGB(0, 255, 127), Particle.VILLAGER_HAPPY, Material.EMERALD_BLOCK));
		register("OBSIDIAN", new SpecialFirework(Material.OBSIDIAN, UnlockMethod.OBSIDIAN, Type.BALL_LARGE, true, true, Color.fromRGB(102, 51, 153), Color.fromRGB(0, 0, 0), Particle.SPELL_WITCH, Material.OBSIDIAN));
		
		register("ZOMBIE", new SpecialFirework(Material.ROTTEN_FLESH, UnlockMethod.SHOP, new FireworkEffect[] {
				FireworkEffect.builder().with(Type.CREEPER).withColor(Color.BLACK).withTrail().build(),
				FireworkEffect.builder().with(Type.BALL_LARGE).withColor(Color.fromRGB(121, 156, 101)).withColor(Color.fromRGB(67, 109, 53))
				.withColor(Color.fromRGB(105, 135, 86)).withColor(Color.fromRGB(140, 37, 16)).build(),
				},Particle.DAMAGE_INDICATOR, 
				"eyJ0ZXh0dXJlcyI6eyJTS0lOIjp7InVybCI6Imh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvNTZmYzg1NGJiODRjZjRiNzY5NzI5Nzk3M2UwMmI3OWJjMTA2OTg0NjBiNTFhNjM5YzYwZTVlNDE3NzM0ZTExIn19fQ=="));
		
		register("PIGMAN", new SpecialFirework(Material.GRILLED_PORK, UnlockMethod.SHOP, new FireworkEffect[] {
				FireworkEffect.builder().with(Type.CREEPER).withColor(Color.BLACK).withTrail().build(),
				FireworkEffect.builder().with(Type.BALL_LARGE).withColor(Color.fromRGB(121, 156, 101)).withColor(Color.fromRGB(67, 109, 53))
				.withColor(Color.fromRGB(151, 87, 87)).withColor(Color.fromRGB(240, 166, 165)).build(),
				},Particle.DAMAGE_INDICATOR, 
				"eyJ0ZXh0dXJlcyI6eyJTS0lOIjp7InVybCI6Imh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvOTVmYjJkZjc1NGM5OGI3NDJkMzVlN2I4MWExZWVhYzlkMzdjNjlmYzhjZmVjZDNlOTFjNjc5ODM1MTZmIn19fQ=="));
		
		register("PRIDE_MONTH", new SpecialFirework(Material.RED_ROSE, 2, UnlockMethod.SHOP, new FireworkEffect[] {
				FireworkEffect.builder().with(Type.STAR).withColor(Color.WHITE).withTrail().build(),
				FireworkEffect.builder().with(Type.BALL_LARGE).withColor(Color.RED).withColor(Color.ORANGE).build(),
				FireworkEffect.builder().with(Type.BALL_LARGE).withColor(Color.YELLOW).withColor(Color.GREEN).build(),
				FireworkEffect.builder().with(Type.BALL_LARGE).withColor(Color.BLUE).withColor(Color.PURPLE).build(),
				},Particle.HEART, 
				"eyJ0ZXh0dXJlcyI6eyJTS0lOIjp7InVybCI6Imh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvZWZkOGJlZDJkZmU0YzMyMTY4Yzk3MjE1NGVlYTMzNWE4MDQyZTlkNjRiODUwNzY3YzZlYTA0Y2U4Zjg1ZjEyYSJ9fX0="));
		
		register("4TH_JULY", new SpecialFirework(Material.FIREWORK, UnlockMethod.SHOP, new FireworkEffect[] {
				FireworkEffect.builder().with(Type.STAR).withColor(Color.fromRGB(191, 10, 48)).withTrail().build(),
				FireworkEffect.builder().with(Type.BALL).withColor(Color.fromRGB(0, 40, 104)).withTrail().build(),
				FireworkEffect.builder().with(Type.BALL_LARGE).withColor(Color.WHITE).build(),
				},Particle.END_ROD, 
				"eyJ0ZXh0dXJlcyI6eyJTS0lOIjp7InVybCI6Imh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvNGNhYzk3NzRkYTEyMTcyNDg1MzJjZTE0N2Y3ODMxZjY3YTEyZmRjY2ExY2YwY2I0YjM4NDhkZTZiYzk0YjQifX19"));
		
		register("SUMMER", new SpecialFirework(Material.WATER_BUCKET, UnlockMethod.SHOP, new FireworkEffect[] {
				FireworkEffect.builder().with(Type.BALL_LARGE).withColor(Color.BLUE).withColor(Color.fromRGB(230, 217, 173)).build(),
				FireworkEffect.builder().with(Type.BURST).withColor(Color.BLUE).build(),
				}, Particle.DRIP_WATER,
				"eyJ0ZXh0dXJlcyI6eyJTS0lOIjp7InVybCI6Imh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvOTNjMmYxYzVkMmM4ZjBlMzM3MzBjMTRkY2ExYzFkMWUxYWJkODU5NmIwODM5ZDY3MzhkMThmNDY0MzJiNmZhNiJ9fX0="));
		
		register("HALLOWEEN_2018", new SpecialFirework(Material.PUMPKIN, UnlockMethod.SHOP_HALLOWEEN_2018, new FireworkEffect[] {
				FireworkEffect.builder().with(Type.CREEPER).withColor(Color.fromRGB(216, 238, 71)).build(),
				FireworkEffect.builder().with(Type.BALL_LARGE).withColor(Color.fromRGB(227, 144, 29)).build(),
				}, Particle.FLAME, Material.PUMPKIN));
		
		register("WINTER", new SpecialFirework(Material.SNOW_BALL, UnlockMethod.CHRISTMAS, new FireworkEffect[] {
				FireworkEffect.builder().with(Type.BALL_LARGE).withColor(Color.WHITE).build(),
				FireworkEffect.builder().with(Type.BALL).withColor(Color.AQUA).build(),
				}, Particle.SNOW_SHOVEL, Material.SNOW_BALL));
		
		register("CHRISTMAS_STAR", new SpecialFirework(Material.NETHER_STAR, UnlockMethod.SHOP_CHRISTMAS, new FireworkEffect[] {
				FireworkEffect.builder().with(Type.STAR).withColor(Color.WHITE).withColor(Color.YELLOW).withFade(Color.ORANGE).build(),
				}, null, new ItemStack(Material.AIR)));
		
		register("CHRISTMAS_2018", new SpecialFirework(Material.WOOL, 14, UnlockMethod.SHOP_CHRISTMAS_2018, new FireworkEffect[] {
				FireworkEffect.builder().with(Type.BALL_LARGE).withColor(Color.WHITE).withColor(Color.RED).build(),
				FireworkEffect.builder().with(Type.BURST).withColor(Color.YELLOW).withTrail().build(),
				}, Particle.SNOW_SHOVEL, 
				"eyJ0ZXh0dXJlcyI6eyJTS0lOIjp7InVybCI6Imh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvNzU5NGNhNWRjNWM4NWRiM2I0YTkwZDQ4NTkzMmJlZGU1ZmJkZjQwMjNmYzRmYmZmNmZlMTRiZTQwOWMxZjk3In19fQ=="));
	}
	
	
	public static List<String> getIds(){
		return SpecialFireworks.fireworksIdOrdered;
	}
	
	public static SpecialFirework getFirework(String id) {
		return SpecialFireworks.fireworks.get(id);
	}
	
	public static Collection<SpecialFirework> getAllFireworks(){
		return SpecialFireworks.fireworks.values();
	}
	
	public static HashMap<String, SpecialFirework> getFireworkMap(){
		return SpecialFireworks.fireworks;
	}
	
	private static void register(String id, SpecialFirework firework) {
		SpecialFireworks.fireworks.put(id.toUpperCase(), firework);
		SpecialFireworks.fireworksIdOrdered.add(id.toUpperCase());
	}
	
	
}
