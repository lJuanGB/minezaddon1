package com.gmail.lJuanGBMinecraft.Fireworks.SpecialFireworks;

import java.lang.reflect.Field;
import java.util.UUID;

import org.bukkit.Color;
import org.bukkit.FireworkEffect;
import org.bukkit.FireworkEffect.Type;
import org.bukkit.Material;
import org.bukkit.Particle;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.SkullMeta;
import org.bukkit.material.MaterialData;

import com.gmail.lJuanGBMinecraft.Fireworks.UnlockMethod;
import com.mojang.authlib.GameProfile;
import com.mojang.authlib.properties.Property;

/**
 * Facilitates creation of special fireworks.
 * 
 * @author lJuanGB
 */
public class SpecialFirework {

	private MaterialData icon;
	private UnlockMethod unlock;
	private FireworkEffect[] effects;
	private Particle particle;
	private ItemStack item;
	
	
	public SpecialFirework(Material icon, UnlockMethod unlock, FireworkEffect[] effects, Particle particle, ItemStack item) {
		
		this.icon = new MaterialData(icon);
		this.unlock = unlock;
		this.effects = effects;
		this.particle = particle;
		this.item = item;
		
	}
	
	public SpecialFirework(Material icon, UnlockMethod unlock, Type type, boolean trail, boolean flicker, Color color, Color fadeColor, Particle particle, ItemStack item) {
		
		this(icon, unlock, 
				new FireworkEffect[] {FireworkEffect.builder().with(type).flicker(flicker).trail(trail).withColor(color).withFade(fadeColor).build()}, 
				particle, item);
		
	}
	
	public SpecialFirework(Material icon, UnlockMethod unlock, Type type, boolean trail, boolean flicker, Color color, Color fadeColor, Particle particle, Material material) {
		this(icon, unlock, type, trail, flicker, color, fadeColor, particle, new ItemStack(material));
	}
	
	public SpecialFirework(Material icon, UnlockMethod unlock, FireworkEffect[] effects, Particle particle, Material material) {
		this(icon, unlock, effects, particle, new ItemStack(material));
	}
	
	public SpecialFirework(Material icon, UnlockMethod unlock, Type type, boolean trail, boolean flicker, Color color, Color fadeColor, Particle particle, String head) {
		this(icon, unlock, type, trail, flicker, color, fadeColor, particle, getHead(head));
	}

	public SpecialFirework(Material icon, UnlockMethod unlock, FireworkEffect[] effects, Particle particle, String head) {
		this(icon, unlock, effects, particle, getHead(head));
	}
	
	@SuppressWarnings("deprecation")
	public SpecialFirework(Material icon, int data, UnlockMethod unlock, FireworkEffect[] effects, Particle particle, ItemStack item) {
		
		this.icon = new MaterialData(icon, (byte) data);
		this.unlock = unlock;
		this.effects = effects;
		this.particle = particle;
		this.item = item;
		
	}
	
	public SpecialFirework(Material icon, int data, UnlockMethod unlock, Type type, boolean trail, boolean flicker, Color color, Color fadeColor, Particle particle, ItemStack item) {
		
		this(icon, data, unlock, 
				new FireworkEffect[] {FireworkEffect.builder().with(type).flicker(flicker).trail(trail).withColor(color).withFade(fadeColor).build()}, 
				particle, item);
		
	}
	
	public SpecialFirework(Material icon, int data, UnlockMethod unlock, Type type, boolean trail, boolean flicker, Color color, Color fadeColor, Particle particle, Material material) {
		this(icon, data, unlock, type, trail, flicker, color, fadeColor, particle, new ItemStack(material));
	}
	
	public SpecialFirework(Material icon, int data, UnlockMethod unlock, FireworkEffect[] effects, Particle particle, Material material) {
		this(icon, data, unlock, effects, particle, new ItemStack(material));
	}
	
	public SpecialFirework(Material icon, int data, UnlockMethod unlock, Type type, boolean trail, boolean flicker, Color color, Color fadeColor, Particle particle, String head) {
		this(icon, data, unlock, type, trail, flicker, color, fadeColor, particle, getHead(head));
	}
	
	public SpecialFirework(Material icon, int data, UnlockMethod unlock, FireworkEffect[] effects, Particle particle, String head) {
		this(icon, data, unlock, effects, particle, getHead(head));
	}
	
	
	
	public UnlockMethod getUnlock() {
		return unlock;
	}
	
	public MaterialData getIcon() {
		return icon;
	}
	
	public FireworkEffect[] getEffects() {
		return this.effects;
	}

	public Particle getParticle() {
		return particle;
	}

	public ItemStack getItem() {
		return item;
	}
	
	
	
	private static ItemStack getHead(String headString) {
		
	 ItemStack head = new ItemStack(Material.SKULL_ITEM, 1, (byte) 3);
	   
	 SkullMeta headMeta = (SkullMeta) head.getItemMeta();
	 
	 GameProfile profile = new GameProfile(UUID.randomUUID(), null);
	   
	 profile.getProperties().put("textures", new Property("textures", headString));
	   
	 try {
		 Field profileField = headMeta.getClass().getDeclaredField("profile");
	     profileField.setAccessible(true);
	     profileField.set(headMeta, profile);
	        
	 } catch (Exception ex){
		 ex.printStackTrace();
	 }
	
	head.setItemMeta(headMeta);
	return head;

	}
	
}
