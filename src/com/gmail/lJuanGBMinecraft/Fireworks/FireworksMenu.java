package com.gmail.lJuanGBMinecraft.Fireworks;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import org.apache.commons.lang.WordUtils;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.DyeColor;
import org.bukkit.FireworkEffect;
import org.bukkit.FireworkEffect.Type;
import org.bukkit.Material;
import org.bukkit.Particle;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.material.MaterialData;
import org.bukkit.material.Wool;
import org.bukkit.metadata.FixedMetadataValue;

import com.gmail.lJuanGBMinecraft.AddonStartUp;
import com.gmail.lJuanGBMinecraft.Fireworks.SpecialFireworks.SpecialFirework;
import com.gmail.lJuanGBMinecraft.Fireworks.SpecialFireworks.SpecialFireworks;

/**
 * The menu for the firework selection with all its submenus. In charge of loading the icons and applying
 * a glow effect if they are selected, and a lore depending on if they are unlocked or not
 * 
 * @param plugin
 * @param player
 */
public class FireworksMenu {
	
	private final AddonStartUp plugin;
	public final Player player;
	public String page = "";
	
	public FireworkEffect.Type type = FireworkEffect.Type.BALL;
	public boolean trail = false;
	public boolean flicker = false;
	public DyeColor color = DyeColor.RED;
	public DyeColor fadeColor = null;
	public Particle particle = null;
	public Material item = null;
	public String special = null;
	

	private final Integer[] borderSmall = new Integer[] {0, 1, 2, 3, 5, 6, 7, 8, 18, 19, 20, 21, 23, 24, 25, 26};
	private final Integer[] borderMedium = new Integer[] {0, 1, 2, 3, 5, 6, 7, 8, 27, 28, 29, 30, 32, 33, 34, 35};
	private final Integer[] borderBig = new Integer[] {0, 1, 2, 3, 5, 6, 7, 8, 36, 37, 38, 39, 41, 42, 43, 44};
	private final Integer[] borderBiggest = new Integer[] {0, 1, 2, 3, 5, 6, 7, 8, 45, 46, 47, 48, 50, 51, 52, 53};
	private ItemStack border;
	private ItemStack returnIcon;
	

	public FireworksMenu(AddonStartUp plugin, Player player) {
		this.plugin = plugin;
		this.player = player;
		
		border = new ItemStack(Material.STAINED_GLASS_PANE);
		ItemMeta metaBorder = border.getItemMeta();
		metaBorder.setDisplayName(" ");
		border.setDurability((short) 7);
		border.setItemMeta(metaBorder);
		
		returnIcon = new ItemStack(Material.EMERALD);
		ItemMeta metaReturn = border.getItemMeta();
		metaReturn.setDisplayName(ChatColor.WHITE + "Return to main menu");
		returnIcon.setItemMeta(metaReturn);
		
		updatePlayerInfo();
		
		player.setMetadata("openFireworkMenu", new FixedMetadataValue(plugin, this));
	}

	
	
	
	
	/**
	 * Loads the data from the config. If there is no file, create it with default values.
	 */
	@SuppressWarnings("deprecation")
	private void updatePlayerInfo() {
		FileConfiguration cnfg = getConfig(player);
		
		if(!cnfg.isInt("type")) {
			cnfg.set("type", getIntFromType(FireworkEffect.Type.BALL));
		}
		this.type = getTypeFromInt(cnfg.getInt("type"));
		
		
		if(!cnfg.isBoolean("trail")) {
			cnfg.set("trail", false);
		}
		this.trail = cnfg.getBoolean("trail");
		
		
		if(!cnfg.isBoolean("flicker")) {
			cnfg.set("flicker", false);
		}
		this.flicker = cnfg.getBoolean("flicker");
		
		
		if(!cnfg.isInt("color")) {
			cnfg.set("color", 14);
		}
		this.color = DyeColor.getByData((byte) cnfg.getInt("color"));
		
		
		if(!cnfg.isInt("fadeColor")) {
			cnfg.set("fadeColor", 99);
		}
		this.fadeColor = DyeColor.getByData((byte) cnfg.getInt("fadeColor"));
		
		
		if(!cnfg.isString("particle")) {
			cnfg.set("particle", "none");
		}
		try {
			this.particle = Particle.valueOf(cnfg.getString("particle"));
		} catch(Exception e) {
			this.particle = null;
		}
		
		
		if(!cnfg.isString("item")) {
			cnfg.set("item", "none");
		}
		try {
			this.item = Material.valueOf(cnfg.getString("item"));
		} catch(Exception e) {
			this.item = null;
		}
		
		
		if(!cnfg.isString("special")) {
			cnfg.set("special", "NONE");
		}
		this.special = cnfg.getString("special");
		
		
		try {
			cnfg.save(new File(plugin.getDataFolder()+"/playerdata", player.getUniqueId() + ".yml"));
		} catch (IOException e) {
			System.out.println(ChatColor.RED + "Failed to save firework player data");
			e.printStackTrace();
		}
	}

	
	private FileConfiguration getConfig(Player player) {
		File file = new File(plugin.getDataFolder()+"/playerdata", player.getUniqueId() + ".yml");
		return YamlConfiguration.loadConfiguration(file);
	}
	
	
	/**
	 * Changes the config of the owner of this menu
	 * 
	 * @param path Where to place the object
	 * @param obj Object to put in the path
	 */
	public void changeConfig(String path, Object obj) {
		FileConfiguration cnfg = getConfig(player);
		
		cnfg.set(path, obj);
		
		try {
			cnfg.save(new File(plugin.getDataFolder()+"/playerdata", player.getUniqueId() + ".yml"));
		} catch (IOException e) {
			System.out.println("Failed to save firework player data");
			e.printStackTrace();
		}
	}

	
	public static Type getTypeFromInt(int i) {
		switch (i) {
		case 0: return FireworkEffect.Type.BALL;
		case 1: return FireworkEffect.Type.BALL_LARGE;
		case 2: return FireworkEffect.Type.BURST;
		case 3: return FireworkEffect.Type.CREEPER;
		case 4: return FireworkEffect.Type.STAR;
		default: return null;
		}
	}
	
	public static int getIntFromType(Type type) {
		if(type.equals(Type.BALL)) return 0;
		if(type.equals(Type.BALL_LARGE)) return 1;
		if(type.equals(Type.BURST)) return 2;
		if(type.equals(Type.CREEPER)) return 3;
		if(type.equals(Type.STAR)) return 4;
		return 0;
	}
	
	
	
	
	
	/**
	 * Open the main page. This creates the inventory or update it if the page is already open.
	 */
	public void openMainPage() {
		
		ItemStack[] icons = loadMainIcons();
		Inventory inv = this.createInventory("Firework Menu", icons);
		
		if(page.equals("Main")) {
			player.getOpenInventory().getTopInventory().setContents(inv.getContents());
		} else {
			player.openInventory(inv);
			this.page = "Main";
		}
		
	}
	
	/**
	 * Open the type page. This creates the inventory or update it if the page is already open.
	 */
	public void openTypePage() {
		
		ItemStack[] icons = loadTypeIcons();
		Inventory inv = this.createInventory("Firework Menu: Explosion Type", icons);
		
		if(page.equals("Type")) {
			player.getOpenInventory().getTopInventory().setContents(inv.getContents());
		} else {
			player.openInventory(inv);
			this.page = "Type";
		}
	}
	
	/**
	 * Open the color page. This creates the inventory or update it if the page is already open.
	 */
	public void openColorPage() {
		
		ItemStack[] icons = loadColorIcons();
		Inventory inv = this.createInventory("Firework Menu: Color", icons);
		
		if(page.equals("Color")) {
			player.getOpenInventory().getTopInventory().setContents(inv.getContents());
		} else {
			player.openInventory(inv);
			this.page = "Color";
		}
	}
	
	/**
	 * Open the fade color page. This creates the inventory or update it if the page is already open.
	 */
	public void openFadeColorPage() {

		ItemStack[] icons = loadFadeColorIcons();
		Inventory inv = this.createInventory("Firework Menu: Fade Color", icons);
		
		if(page.equals("FadeColor")) {
			player.getOpenInventory().getTopInventory().setContents(inv.getContents());
		} else {
			player.openInventory(inv);
			this.page = "FadeColor";
		}
	}
	
	/**
	 * Open the particle page. This creates the inventory or update it if the page is already open.
	 */
	public void openParticlePage() {
		
		ItemStack[] icons = loadParticlesIcons();
		Inventory inv = this.createInventory("Firework Menu: Particles", icons);
		
		if(page.equals("Particle")) {
			player.getOpenInventory().getTopInventory().setContents(inv.getContents());
		} else {
			player.openInventory(inv);
			this.page = "Particle";
		}
	}


	/**
	 * Open the items page. This creates the inventory or update it if the page is already open.
	 */
	public void openItemsPage() {
		
		ItemStack[] icons = loadItemsIcons();
		Inventory inv = this.createInventory("Firework Menu: Items", icons);
		
		if(page.equals("Items")) {
			player.getOpenInventory().getTopInventory().setContents(inv.getContents());
		} else {
			player.openInventory(inv);
			this.page = "Items";
		}
	}
	
	/**
	 * Open the special page. This creates the inventory or update it if the page is already open.
	 */
	public void openSpecialPage() {
		
		ItemStack[] icons = loadSpecialIcons();
		Inventory inv = this.createInventory("Firework Menu: Special Fireworks", icons);
		
		if(page.equals("Special")) {
			player.getOpenInventory().getTopInventory().setContents(inv.getContents());
		} else {
			player.openInventory(inv);
			this.page = "Special";
		}
	}
	
	
	private Inventory createInventory(String menuName, ItemStack[] icons) {
		
		Integer[] borderArray = new Integer[] {};
		int returnPosition = 0;
		int invSize = 0;
		int iconsLength = icons.length-1;
		
		if(0 <= iconsLength && iconsLength <= 9) {
			borderArray = borderSmall;
			returnPosition = 22;
			invSize = 27;
		}
		if(10 <= iconsLength && iconsLength <= 18) {
			borderArray = borderMedium;
			returnPosition = 31;
			invSize = 36;
		}
		if(19 <= iconsLength && iconsLength <= 27) {
			borderArray = borderBig;
			returnPosition = 40;
			invSize = 45;
		}
		if(28 <= iconsLength && iconsLength <= 36) {
			borderArray = borderBiggest;
			returnPosition = 49;
			invSize = 54;
		}
		
		Inventory inv = Bukkit.createInventory(null, invSize, menuName);
		
		for(Integer n : borderArray) {
			inv.setItem(n, border);
		}
		
		for(int i = 1; i < icons.length; i++) {
			inv.setItem(i+8, icons[i]);
		}
		
		inv.setItem(4, icons[0]);
		inv.setItem(returnPosition, returnIcon);
		
		return inv;
	}
	
	
	
	
	
	private ItemStack[] loadMainIcons() {
		updatePlayerInfo();
		
		ItemStack title = createIcon(Material.FIREWORK, "Change your firework", "Click an icon to select the option","or to open the corresponding submenu");
		
		ItemStack iconType = createIcon(Material.FIREWORK_CHARGE, "Type of explosion", "Choose between five types of explosion");

		ItemStack iconTrail = createUnlockableIcon(Material.DIAMOND, "Trail", trail, "trail", UnlockMethod.SHOP);
		
		ItemStack iconFlicker = createUnlockableIcon(Material.GLOWSTONE_DUST, "Flicker", flicker, "flicker", UnlockMethod.SHOP);	
		
		ItemStack iconColor = createIcon(Material.WOOL, "Main Color", "Choose between the 16 minecraft colors");
		
		ItemStack iconFadeColor =createIcon(Material.STAINED_GLASS, "Fade Color", "Choose between the 16 minecraft colors");
		
		ItemStack iconParticles = createIcon(Material.NOTE_BLOCK, "Particles", "Add extra particles to the firework");
		
		ItemStack iconItem = createIcon(Material.APPLE, "Items", "Chose what items the firework will spawn", ChatColor.BOLD + "(COSMETIC)");
		
		ItemStack iconSpecial = createIcon(Material.EXP_BOTTLE, "Special Fireworks", "Choose special predefined fireworks");
		
		return new ItemStack[] {title, iconType, iconTrail, iconFlicker, iconColor, iconFadeColor, iconParticles, iconItem, iconSpecial};
	}
	
	
	private ItemStack[] loadTypeIcons() {
		
		updatePlayerInfo();
		
		List<ItemStack> icons = new ArrayList<ItemStack>();
		
		icons.add( createIcon(Material.FIREWORK_CHARGE, "Type of explosion", "Choose between five types of explosion") );

		for(Type type : Type.values()) {
			Material material = null;
			
			switch(type) {
			case BALL:
				material = Material.SULPHUR;
				break;
			case BALL_LARGE:
				material = Material.TNT;
				break;
			case BURST:
				material = Material.FEATHER;
				break;
			case CREEPER:
				material = Material.GREEN_RECORD;
				break;
			case STAR:
				material = Material.GOLD_NUGGET;
				break;
			}
			String name = WordUtils.capitalizeFully(type.name().replaceAll("_", " "));
			boolean glow = (this.type != null && this.type.equals(type));
			
			icons.add( createUnlockableIcon(material, name, glow, "type." + type.name().toLowerCase(), UnlockMethod.SHOP));
			
		}
		
		return icons.toArray(new ItemStack[] {});
	}
	

	private ItemStack[] loadColorIcons() {
		updatePlayerInfo();
		
		List<ItemStack> icons = new ArrayList<ItemStack>();
		
		icons.add( createIcon(Material.WOOL, "Main Color", "Choose between the 16 minecraft colors") );

		for(DyeColor color : DyeColor.values()) {
			String name = WordUtils.capitalizeFully(color.name().replaceAll("_", " "));
			boolean glow = (this.color != null && this.color.equals(color));
			Wool material = new Wool(color);
			
			icons.add( createUnlockableIcon(material, name, glow, "color." + color.name().toLowerCase(), UnlockMethod.SHOP));
			
		}
		
		return icons.toArray(new ItemStack[] {});		
	}
	
	
	@SuppressWarnings("deprecation")
	private ItemStack[] loadFadeColorIcons() {
		updatePlayerInfo();
		
		List<ItemStack> icons = new ArrayList<ItemStack>();
		
		icons.add( createIcon(Material.STAINED_GLASS, "Fade Color", "Choose between the 16 minecraft colors") );
		icons.add( createSelectableIcon(Material.BARRIER, "None", (fadeColor == null)) );
		
		for(DyeColor color : DyeColor.values()) {
			String name = WordUtils.capitalizeFully(color.name().replaceAll("_", " "));
			boolean glow = (this.fadeColor != null && this.fadeColor.equals(color));
			MaterialData material = new MaterialData(Material.STAINED_GLASS, color.getData());
			
			icons.add( createUnlockableIcon(material, name, glow, "fadecolor." + color.name().toLowerCase(), UnlockMethod.SHOP));
			
		}
		
		return icons.toArray(new ItemStack[] {});
	}

	
	private ItemStack[] loadParticlesIcons() {
		
		updatePlayerInfo();
		
		List<ItemStack> icons = new ArrayList<ItemStack>();
		
		icons.add( createIcon(Material.NOTE_BLOCK, "Particles", "Add extra particles to the firework") );
		icons.add( createSelectableIcon(Material.BARRIER, ChatColor.RED + "None", (particle == null)) );
		
		for(Particle particle : Particle.values()) {
			Material material = null;
			
			switch(particle) {
			case NOTE:
				material = Material.NOTE_BLOCK;
				break;
			case WATER_SPLASH:
				material = Material.WATER_BUCKET;
				break;
			case SNOWBALL:
				material = Material.SNOW_BALL;
				break;
			case EXPLOSION_NORMAL:
				material = Material.TNT;
				break;
			case VILLAGER_HAPPY:
				material = Material.EMERALD;
				break;
			case LAVA:
				material = Material.LAVA_BUCKET;
				break;
			case FLAME:
				material = Material.BLAZE_POWDER;
				break;
			case REDSTONE:
				material = Material.REDSTONE;
				break;
			case DRAGON_BREATH:
				material = Material.DRAGONS_BREATH;
				break;
			case ENCHANTMENT_TABLE:
				material = Material.BOOKSHELF;
				break;
			case END_ROD:
				material = Material.END_ROD;
				break;
			case SPELL_WITCH:
				material = Material.RED_MUSHROOM;
				break;
			default:
				continue;
			}
			
			String name = WordUtils.capitalizeFully(particle.name().replaceAll("_", " "));
			boolean glow = (this.particle != null && this.particle.equals(particle));
			
			icons.add( createUnlockableIcon(material, name, glow, "particle." + particle.name().toLowerCase(), UnlockMethod.SHOP));
			
		}
		
		return icons.toArray(new ItemStack[] {});
	}

	private ItemStack[] loadItemsIcons() {
		
		updatePlayerInfo();
		
		List<ItemStack> icons = new ArrayList<ItemStack>();
		
		icons.add( createIcon(Material.APPLE, "Items", "Chose what items the firework will spawn", ChatColor.BOLD + "(COMETIC)") );
		icons.add( createSelectableIcon(Material.BARRIER, ChatColor.RED + "None", (item == null)) );
		
		for(Material material : Material.values()) {
			
			switch(material) {
			case APPLE:
			case RAW_FISH:
			case COOKIE:
			case ROTTEN_FLESH:
			case PAPER:
			case MILK_BUCKET:
			case BRICK:
			case PUMPKIN_PIE:
			case WOOD_HOE:
			case WEB:
			case RED_ROSE:
			case ENDER_PEARL:
				break;
			default:
				continue;
			}
			
			String name = WordUtils.capitalizeFully(material.name().replaceAll("_", " "));
			boolean glow = (this.item != null && this.item.equals(material));
			
			icons.add( createUnlockableIcon(material, name, glow, "item." + material.name().toLowerCase(), UnlockMethod.SHOP));
			
		}
		
		return icons.toArray(new ItemStack[] {});
	}
	
	private ItemStack[] loadSpecialIcons() {
		
		updatePlayerInfo();

		List<ItemStack> icons = new ArrayList<ItemStack>();
		
		icons.add( createIcon(Material.EXP_BOTTLE, "Special Fireworks", "Choose special predefined fireworks") );
		icons.add( createSelectableIcon(Material.BARRIER, ChatColor.RED + "None", (special == null || special.equals("NONE"))) );
		
		HashMap<String, SpecialFirework> specials = SpecialFireworks.getFireworkMap();
		
		for(String id : SpecialFireworks.getIds()) {
			
			String name = WordUtils.capitalizeFully(id.replaceAll("_", " "));
			boolean glow = (this.special != null && this.special.equals(id));
			SpecialFirework sp = specials.get(id);
			
			icons.add( createUnlockableIcon(sp.getIcon(), name, glow, "item." + id.toLowerCase(), sp.getUnlock()));
		
		}
		
		return icons.toArray(new ItemStack[] {});
	}
	
	
	
	
	
	private ItemStack createIcon(MaterialData material, String name, String... lore) {
		ItemStack result = material.toItemStack(1);
		ItemMeta meta = result.getItemMeta();
		
		meta.setDisplayName(ChatColor.WHITE + name);
		
		if(lore.length > 0) {
			List<String> loreList = new ArrayList<String>();
			loreList.add("");
			
			for(String s : lore) {
				loreList.add(ChatColor.GRAY + s);
			}
			
			meta.setLore(loreList);
		}
			
		result.setItemMeta(meta);
		return result;
	}
	
	private ItemStack createIcon(Material material, String name, String... lore) {
		return createIcon(new MaterialData(material), name, lore);
	}
	
	
	private ItemStack createSelectableIcon(MaterialData material, String name, boolean glow) {
		
		ItemStack result = createIcon(material, name);
		ItemMeta meta = result.getItemMeta();
		
		meta.addItemFlags(ItemFlag.HIDE_ENCHANTS);
		if(glow) meta.addEnchant(Enchantment.MENDING, 1, true);
		
		result.setItemMeta(meta);
		return result;
	}
	
	private ItemStack createSelectableIcon(Material material, String name, boolean glow) {
		return createSelectableIcon(new MaterialData(material), name, glow);
	}
	
	
	private ItemStack createUnlockableIcon(MaterialData material, String name, boolean glow, boolean unlocked, UnlockMethod method) {
		
		ItemStack result = createSelectableIcon(material, name, glow);
		ItemMeta meta = result.getItemMeta();
		
		if(unlocked) {
			meta.setLore(Arrays.asList(new String[] {"", ChatColor.GREEN + "UNLOCKED"}));
		} else {
			meta.setLore(Arrays.asList(new String[] {"", method.getDisplay()}));
		}
		
		result.setItemMeta(meta);
		return result;
	}

	
	private ItemStack createUnlockableIcon(MaterialData material, String name, boolean glow, String permission, UnlockMethod method) {
		boolean unlocked = false;
		
		if(this.player.hasPermission("minez.addon1.fireworks." + permission)) unlocked = true;
		
		return createUnlockableIcon(material, name, glow, unlocked, method);
	}
	
	private ItemStack createUnlockableIcon(Material material, String name, boolean glow, String permission, UnlockMethod method) {
		return createUnlockableIcon(new MaterialData(material), name, glow, permission, method);
	}
}
