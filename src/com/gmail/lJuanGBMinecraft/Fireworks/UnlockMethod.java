package com.gmail.lJuanGBMinecraft.Fireworks;

import org.bukkit.ChatColor;

public enum UnlockMethod {

	SHOP(ChatColor.GRAY + "Unlock in the Shotbow Shop!"),
	CRATE(ChatColor.GRAY + "Unlock by opening " + ChatColor.WHITE + "Cosmetic Crates"),
	
	SILVER(ChatColor.GRAY + "Unlock by becoming " + ChatColor.WHITE + "SILVER" + ChatColor.GRAY + " in the shop."),
	GOLD(ChatColor.GRAY + "Unlock by becoming " + ChatColor.GOLD + "GOLD" + ChatColor.GRAY + " in the shop."),
	PLATINUM(ChatColor.GRAY + "Unlock by becoming " + ChatColor.AQUA + "PLATINUM" + ChatColor.GRAY + " in the shop."),
	EMERALD(ChatColor.GRAY + "Unlock by becoming " + ChatColor.GREEN + "EMERALD"),
	OBSIDIAN(ChatColor.GRAY + "Unlock by becoming " + ChatColor.DARK_PURPLE + "OBSIDIAN"),
	
	GENERIC_EVENT(ChatColor.GRAY + "Unlock by participating in an event!"),
	CHRISTMAS(ChatColor.GRAY + "Unlock by participating in "+ ChatColor.RED +"Christmas Events!"),
	CHRISTMAS_2018(ChatColor.GRAY + "Unlock by participating in the "+ ChatColor.RED +"Christmas Events" + ChatColor.GRAY + " of 2018!"),
	CHRISTMAS_2019(ChatColor.GRAY + "Unlock by participating in the "+ ChatColor.RED +"Christmas Events" + ChatColor.GRAY + " of 2019!"),
	HALLOWEEN(ChatColor.GRAY + "Unlock by participating in "+ ChatColor.GOLD +"Halloween Events!"),
	HALLOWEEN_2018(ChatColor.GRAY + "Unlock by participating in the "+ ChatColor.GOLD +"Halloween Events" + ChatColor.GRAY + "of 2018!"),
	HALLOWEEN_2019(ChatColor.GRAY + "Unlock by participating in the "+ ChatColor.GOLD +"Halloween Events" + ChatColor.GRAY + "of 2019!"),
	
	SHOP_CHRISTMAS(ChatColor.GRAY + "Unlock in the Shotbow Shop during "+ ChatColor.RED +"Christmas"),
	SHOP_CHRISTMAS_2018(ChatColor.GRAY + "Unlock in the Shotbow Shop during the "+ ChatColor.RED +"Christmas " + ChatColor.GRAY + "of 2018!"),
	SHOP_CHRISTMAS_2019(ChatColor.GRAY + "Unlock in the Shotbow Shop during the "+ ChatColor.RED +"Christmas" + ChatColor.GRAY + " of 2019!"),
	SHOP_HALLOWEEN(ChatColor.GRAY + "Unlock in the Shotbow Shop during "+ ChatColor.GOLD +"Halloween!"),
	SHOP_HALLOWEEN_2018(ChatColor.GRAY + "Unlock in the Shotbow Shop during the "+ ChatColor.GOLD +"Halloween " + ChatColor.GRAY + "of 2018!"),
	SHOP_HALLOWEEN_2019(ChatColor.GRAY + "Unlock in the Shotbow Shop during the "+ ChatColor.GOLD +"Halloween Events" + ChatColor.GRAY + "of 2019!"),
	
	MISTERY(ChatColor.GRAY + "Unlock by " + ChatColor.MAGIC + "MagicalStuff")
	
	;

	
	private String display;
	
	private UnlockMethod(String display){
		this.display = display;
	}
	
	public String getDisplay() {
		return this.display;
	}
}
