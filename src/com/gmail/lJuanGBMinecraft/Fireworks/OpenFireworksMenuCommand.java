package com.gmail.lJuanGBMinecraft.Fireworks;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import com.gmail.lJuanGBMinecraft.AddonStartUp;

/**
 * Command to manually open the firework menu
 * @param plugin
 */
public class OpenFireworksMenuCommand implements CommandExecutor{

	private final AddonStartUp plugin;

	public OpenFireworksMenuCommand(AddonStartUp plugin) {
		this.plugin = plugin;
	}
	
	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String cmdName, String[] args) {
		if (!(sender instanceof Player)) return false;
		
		Player player = (Player) sender;
		
		if(cmd.getName().equals("firework")) {
			if(!player.hasPermission("minez.addon1.fireworks.openUI")) {
				player.sendMessage(ChatColor.RED + "You don't have permission for that.");
				return false;
			}
			
			FireworksMenu menu = new FireworksMenu(plugin, player);	
			menu.openMainPage();
		}
		return false;
	}

}
