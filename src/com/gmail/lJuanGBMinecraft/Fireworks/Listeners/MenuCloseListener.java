package com.gmail.lJuanGBMinecraft.Fireworks.Listeners;

import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.event.inventory.InventoryType;

import com.gmail.lJuanGBMinecraft.AddonStartUp;

/**
 * If the player has a menu open and they close it, after 1 tick check if another menu is open.
 * If not, remove the player from the map. The 0 tick delay has to be done so that the menu is not
 * removed when the menu changes page (which bukkit considers as inventory closing).
 * )
 * @param plugin
 */
public class MenuCloseListener implements Listener{
	
	private AddonStartUp plugin;

	public MenuCloseListener(AddonStartUp plugin) {
		this.plugin = plugin;
	}
	
	@EventHandler
	public void onPlayerCloseInventory(InventoryCloseEvent e) {
		
		if(e.getPlayer().hasMetadata("openFireworkMenu")) {
			
			plugin.getServer().getScheduler().scheduleSyncDelayedTask(plugin, new Runnable() {
				
				  public void run() {
					  
					  if(!e.getPlayer().getOpenInventory().getType().equals(InventoryType.CHEST)) {
						  e.getPlayer().removeMetadata("openFireworkMenu", plugin);
					  }
					  
				  }
				  
				}, 0L);
			
		}
	}

}
