package com.gmail.lJuanGBMinecraft.Fireworks.Listeners;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.metadata.FixedMetadataValue;

import com.gmail.lJuanGBMinecraft.AddonStartUp;

/**
 * Listens to when a player launches a firework and gives it the necessary data (if its a custom firework)
 * 
 * @author lJuanGB
 */
public class LaunchEventListener implements Listener{
	
	private AddonStartUp plugin;

	public LaunchEventListener(AddonStartUp plugin) {
		this.plugin = plugin;
	}
	
	@EventHandler
	public void onPlayerLaunchFirework(PlayerInteractEvent e) {
		//Check if its a righclick with a custom firework
		if(!e.getAction().equals(Action.RIGHT_CLICK_BLOCK)) return;
		
		if(e.getItem() == null || 
		   !e.getItem().getType().equals(Material.FIREWORK) || 
		   !e.getItem().hasItemMeta() ||
		   !e.getItem().getItemMeta().hasDisplayName() ||
		   !e.getItem().getItemMeta().getDisplayName().contains("Custom Firework")) return;
		
		
		//Check if the player has recently launched a firework. If so, cancel the launch
		if(e.getPlayer().hasMetadata("recentlyLaunchedFirework")) {
			e.getPlayer().sendMessage(ChatColor.GOLD + "Firework launching is in cooldown (" + (int) (plugin.getConfig().getInt("fireworkdelay")/20)+ "s)");
			e.setCancelled(true);
			return;
		}
		
		
		
		//Flag the firework with the owner as soon as its created (1 tick after the interact event is called)
		plugin.getServer().getScheduler().scheduleSyncDelayedTask(plugin, new Runnable() {
			  public void run() {
				  
				  for(Entity en: e.getPlayer().getNearbyEntities(6, 6, 6)) {
					  if(!en.getType().equals(EntityType.FIREWORK)) continue;
					  if(en.getTicksLived() > 1) continue;
					  if(en.hasMetadata("owner")) continue;
					  
					  en.setMetadata("owner", new FixedMetadataValue(plugin, e.getPlayer().getUniqueId()));
				  }
			  }
			}, 1);
		
		
		
		//Flag the player as recently launched and create a task to remove it after the defined ticks in the config.
		if(!e.getPlayer().hasPermission("minez.addon1.fireworks.nodelay")) {
			
			e.getPlayer().setMetadata("recentlyLaunchedFirework", new FixedMetadataValue(plugin, true));
			
			plugin.getServer().getScheduler().scheduleSyncDelayedTask(plugin, new Runnable() {
				  public void run() {
					  e.getPlayer().removeMetadata("recentlyLaunchedFirework", plugin);
				  }
				}, plugin.getConfig().getInt("fireworkdelay"));
		}
	}

}
