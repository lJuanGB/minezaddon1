package com.gmail.lJuanGBMinecraft.Fireworks.Listeners;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.UUID;

import org.bukkit.Color;
import org.bukkit.DyeColor;
import org.bukkit.FireworkEffect;
import org.bukkit.Particle;
import org.bukkit.FireworkEffect.Type;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Firework;
import org.bukkit.entity.Item;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.FireworkExplodeEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.FireworkMeta;
import org.bukkit.inventory.meta.ItemMeta;

import com.gmail.lJuanGBMinecraft.AddonStartUp;
import com.gmail.lJuanGBMinecraft.Fireworks.FireworksMenu;
import com.gmail.lJuanGBMinecraft.Fireworks.SpecialFireworks.SpecialFirework;
import com.gmail.lJuanGBMinecraft.Fireworks.SpecialFireworks.SpecialFireworks;

/**
 * Listens to when a custom firework explodes and makes it have all the effects.
 * 
 * @author lJuanGB
 */
public class ExplodeEventListener implements Listener{

	private AddonStartUp plugin;

	public ExplodeEventListener(AddonStartUp plugin) {
		this.plugin = plugin;
	}
	
	@SuppressWarnings("deprecation")
	@EventHandler
	public void onFireworkExplode(FireworkExplodeEvent e) {
		
		//if the firework has been flagged with an owner, retrieve the owners configuration
		if(!e.getEntity().hasMetadata("owner")) return;
		
		Firework fw = e.getEntity();
		UUID uuid = UUID.fromString(fw.getMetadata("owner").get(0).asString());
		
		File file = new File(plugin.getDataFolder()+"/playerdata", uuid + ".yml");
		FileConfiguration cnfg = YamlConfiguration.loadConfiguration(file);


		//if the player has a special selected, create the special firework and omit the regular creation
		String special = cnfg.isString("special") ? cnfg.getString("special") : "NONE";
		
		if(!special.equals("NONE")) {
			SpecialFirework sp = SpecialFireworks.getFirework(special.toUpperCase());
			
			spawnFirework(fw.getLocation(), sp.getEffects());
			spawnParticles(fw.getLocation(), sp.getParticle());
			spawnItems(fw.getLocation(), sp.getItem());
			return;
		}
		
		
		//Read the necessary values. FadeColor, particle and item may have values that fail, so they must be checked first
		FireworkEffect.Type type = cnfg.isInt("type") ? FireworksMenu.getTypeFromInt(cnfg.getInt("type")) : FireworkEffect.Type.BALL;
		boolean trail = cnfg.isBoolean("trail") ? cnfg.getBoolean("trail") : false;
		boolean flicker = cnfg.isBoolean("flicker") ? cnfg.getBoolean("flicker") : false;
		Color color = cnfg.isInt("color") ? DyeColor.getByData((byte) cnfg.getInt("color")).getColor() : DyeColor.RED.getColor();
		
		Color fadeColor = null;
		if(cnfg.isInt("fadeColor")) {
			if(0 <= cnfg.getInt("fadeColor") && cnfg.getInt("fadeColor") <= 15) {
				fadeColor = DyeColor.getByData((byte) cnfg.getInt("fadeColor")).getColor();
			}
		}
		
		Particle particle = null;
		if(cnfg.isString("particle")) {
			try {
				particle = Particle.valueOf(cnfg.getString("particle"));
			} catch(Exception ex) {
				//ignore
			}
		}
		
		ItemStack item = null;
		Material material= null;
		if(cnfg.isString("item")) {
			try {
				material = Material.valueOf(cnfg.getString("item"));
				item = new ItemStack(material);
			} catch(Exception ex) {
				//ignore
			}
		}
		
		
		spawnFirework(fw.getLocation(), type, trail, flicker, color, fadeColor);
		spawnParticles(fw.getLocation(), particle);
		spawnItems(fw.getLocation(), item);
	}

	
	
	private void spawnFirework(Location location, Type type, boolean trail, boolean flicker, Color color, Color fadeColor) {
		if(color == null) color = DyeColor.RED.getColor();
		if(type == null) type = FireworkEffect.Type.BALL;
		if(fadeColor == null) fadeColor = color;
		
		FireworkEffect effect = FireworkEffect.builder().with(type).withColor(color).withFade(fadeColor).flicker(flicker).trail(trail).build();
		
		this.spawnFirework(location, new FireworkEffect[] {effect});
	}
	
	private void spawnFirework(Location location, FireworkEffect[] effects) {
		if(effects == null) return;
		if(effects.length == 0) return;
				
		//Instead of changing our firework, we create another and in 2 ticks we make it explode.
		//It is a bit hacky, but it is the most reliable option.
		Firework fw = location.getWorld().spawn(location, Firework.class);
		FireworkMeta meta = fw.getFireworkMeta();
		meta.addEffects(effects);
		meta.setPower(1);
		fw.setFireworkMeta(meta);
		plugin.getServer().getScheduler().scheduleSyncDelayedTask(plugin, new Runnable() {
			
			  public void run() {
				  fw.detonate();
			  }
			  
		}, 2);
		
	}

	
	private void spawnParticles(Location location, Particle particle) {
		if(particle == null) return;
		
		location.getWorld().spawnParticle(particle, location, 500, 3, 3, 3, 0.01);
	}

	
	private void spawnItems(Location location, ItemStack item) {
		if(item == null) return;
		if(item.getType().equals(Material.AIR)) return;
		
		Random generator = new Random();
		//Spawn 8 items with random numbers so they dont stack. Make them un pickable. Create a task to remove them.
		List<Item> itemsToRemove = new ArrayList<Item>();
		for(int i = 0; i<=8; i++) {
			ItemMeta meta = item.getItemMeta();
			meta.setDisplayName(generator.nextInt() + "");
			Item itemEntity = location.getWorld().dropItemNaturally(location, item);
			itemEntity.setPickupDelay(32767);
			itemsToRemove.add(itemEntity);

		}
		plugin.getServer().getScheduler().runTaskLater(plugin, new Runnable() {
			
			  public void run() {
				  for(Item itemEntity : itemsToRemove) {
					  itemEntity.remove();
				  }
			  }
			  
		}, 10*20);
	}

}
