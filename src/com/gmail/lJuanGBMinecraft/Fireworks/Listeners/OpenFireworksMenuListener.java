package com.gmail.lJuanGBMinecraft.Fireworks.Listeners;

import org.bukkit.Material;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;

import com.gmail.lJuanGBMinecraft.AddonStartUp;
import com.gmail.lJuanGBMinecraft.Fireworks.FireworksMenu;

/**
 * Open the menu when right clicking with a custom firework in the air
 */
public class OpenFireworksMenuListener implements Listener{
	
	private AddonStartUp plugin;

	public OpenFireworksMenuListener(AddonStartUp plugin) {
		this.plugin = plugin;
	}
	
	@EventHandler
	public void onPlayerLaunchFirework(PlayerInteractEvent e) {
		if(!e.getAction().equals(Action.RIGHT_CLICK_AIR)) return;
		
		if(e.getItem() == null || 
		   !e.getItem().getType().equals(Material.FIREWORK) || 
		   !e.getItem().hasItemMeta() ||
		   !e.getItem().getItemMeta().hasDisplayName() ||
		   !e.getItem().getItemMeta().getDisplayName().contains("Custom Firework")) return;
		
		FireworksMenu menu = new FireworksMenu(plugin, e.getPlayer());	
		menu.openMainPage();
	}
}
