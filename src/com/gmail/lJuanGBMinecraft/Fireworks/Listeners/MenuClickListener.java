package com.gmail.lJuanGBMinecraft.Fireworks.Listeners;

import org.bukkit.ChatColor;
import org.bukkit.DyeColor;
import org.bukkit.Material;
import org.bukkit.Particle;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;

import com.gmail.lJuanGBMinecraft.Fireworks.FireworksMenu;

/**
 * In charge of detecting what item is clicked, and if they have the permission, change the player configuration accordingly.
 * The player has to have a menu open (retrieved from the FireworksMenuMap class) and a valid page open
 */
public class MenuClickListener implements Listener{
	
	
	@SuppressWarnings("deprecation")
	@EventHandler
	public void onPlayerClick(InventoryClickEvent e) {
		FireworksMenu menu = null;
		
		if(e.getWhoClicked().hasMetadata("openFireworkMenu")) {
			menu = (FireworksMenu) e.getWhoClicked().getMetadata("openFireworkMenu").get(0).value();
			e.setCancelled(true);
		} else {
			return;
		}
		
		if(e.getCurrentItem() == null || !e.getCurrentItem().hasItemMeta() || !e.getCurrentItem().getItemMeta().hasDisplayName()) return;
		String clicked = e.getCurrentItem().getItemMeta().getDisplayName();
		
		
		if(clicked.contains("Return to main menu")) {
			menu.openMainPage();
			return;
		}
		
		switch (menu.page) {
		case "Main": {
						if(clicked.contains("Type of explosion")) {
							menu.openTypePage();
							return;
						}
						
						if(clicked.contains("Trail") && menu.player.hasPermission("minez.addon1.fireworks.trail")) {
							if(menu.trail) {
								menu.changeConfig("trail", false);
							} else {
								menu.changeConfig("trail", true);
							}
							menu.openMainPage();
							return;
						}
						
						if(clicked.contains("Flicker") && menu.player.hasPermission("minez.addon1.fireworks.flicker")) {
							if(menu.flicker) {
								menu.changeConfig("flicker", false);
							} else {
								menu.changeConfig("flicker", true);
							}
							menu.openMainPage();
							return;
						}
						
						if(clicked.contains("Main Color")) {
							menu.openColorPage();
							return;
						}
						
						if(clicked.contains("Fade Color")) {
							menu.openFadeColorPage();
							return;
						}
						
						if(clicked.contains("Particles")) {
							menu.openParticlePage();
							return;
						}
						
						if(clicked.contains("Items")) {
							menu.openItemsPage();
							return;
						}
						
						if(clicked.contains("Special Fireworks")) {
							menu.openSpecialPage();
							return;
						}
						
						break;
		}
		case "Type": {
						if(clicked.contains("Ball Large") && menu.player.hasPermission("minez.addon1.fireworks.type.ball_large")) {
							menu.changeConfig("type", 1);
							menu.openTypePage();
							return;
						}
			
						if(clicked.contains("Ball") && menu.player.hasPermission("minez.addon1.fireworks.type.ball")) {
							menu.changeConfig("type", 0);
							menu.openTypePage();
							return;
						}
						
						if(clicked.contains("Burst") && menu.player.hasPermission("minez.addon1.fireworks.type.burst")) {
							menu.changeConfig("type", 2);
							menu.openTypePage();
							return;
						}
						
						if(clicked.contains("Creeper") && menu.player.hasPermission("minez.addon1.fireworks.type.creeper")) {
							menu.changeConfig("type", 3);
							menu.openTypePage();
							return;
						}
						
						if(clicked.contains("Star") && menu.player.hasPermission("minez.addon1.fireworks.type.star")) {
							menu.changeConfig("type", 4);
							menu.openTypePage();
							return;
						}
						
						break;
		}
		case "Color": {
						DyeColor color;
						try {
							color = DyeColor.valueOf( ChatColor.stripColor( clicked.toUpperCase().replaceAll(" ", "_") ) );
						} catch (IllegalArgumentException ex) {
							//ignore, as it was a click in the border / title
							return;
						}
						if(menu.player.hasPermission("minez.addon1.fireworks.color." + color.name().toLowerCase())) {
							menu.changeConfig("color", color.getData());
							menu.openColorPage();
							return;
						}
						break;
		}
		case "FadeColor": {
						if(clicked.contains("None")) {
							menu.changeConfig("fadeColor", 99);
							menu.openFadeColorPage();
							return;
						}
						
						DyeColor color;
						try{
							color = DyeColor.valueOf( ChatColor.stripColor( clicked.toUpperCase().replaceAll(" ", "_") ) );
						} catch (IllegalArgumentException ex) {
							//ignore, as it was a click in the border / title
							return;
						}
						if(menu.player.hasPermission("minez.addon1.fireworks.fadecolor." + color.name().toLowerCase())) {
							menu.changeConfig("fadeColor", color.getData());
							menu.openFadeColorPage();
							return;
						}
						break;
		}
		case "Particle": {
						if(clicked.contains("None")) {
							menu.changeConfig("particle", "NONE");
							menu.openParticlePage();
							return;
						}
						
						Particle particle;
						try {
							particle = Particle.valueOf( ChatColor.stripColor( clicked.toUpperCase().replaceAll(" ", "_") ) );
						} catch (IllegalArgumentException ex) {
							//ignore, as it was a click in the border / title
							return;
						}
						if(menu.player.hasPermission("minez.addon1.fireworks.particle." + particle.name().toLowerCase())) {
							menu.changeConfig("particle", particle.name());
							menu.openParticlePage();
							return;
						}
						break;
		}
		case "Items": {
						if(clicked.contains("None")) {
							menu.changeConfig("item", "NONE");
							menu.openItemsPage();
							return;
						}
						
						Material item;
						try {
							item = Material.valueOf( ChatColor.stripColor( clicked.toUpperCase().replaceAll(" ", "_") ) );
						} catch (IllegalArgumentException ex) {
							//ignore, as it was a click in the border / title
							return;
						}
						if(menu.player.hasPermission("minez.addon1.fireworks.item." + item.name().toLowerCase())) {
							menu.changeConfig("item", item.name());
							menu.openItemsPage();
							return;
						}
						break;
							
		}
		case "Special": {
						if(clicked.contains("None")) {
							menu.changeConfig("special", "NONE");
							menu.openSpecialPage();
							return;
						}
						if(menu.player.hasPermission("minez.addon1.fireworks.special." + clicked.toLowerCase().replace(" ", "_"))) {
							menu.changeConfig("special", ChatColor.stripColor( clicked.toUpperCase().replaceAll(" ", "_")) );
							menu.openSpecialPage();
							return;
						}
						break;
		}
		}
	}
}
