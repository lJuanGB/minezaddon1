package com.gmail.lJuanGBMinecraft;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.PlayerInventory;
import org.bukkit.inventory.meta.ItemMeta;

/**
 * Allows the player to rename the item they are holding if they have nametags in the inventory and the permission.
 * 
 * @author lJuanGB
 */
public class RenameCommand implements CommandExecutor{
	
	private final AddonStartUp plugin;
	
	public RenameCommand(AddonStartUp plugin) {
		this.plugin = plugin;
	}
	
	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String commandName, String[] args) {
		if (!(sender instanceof Player)) return false;
		
		Player player = (Player) sender;
		
		if(cmd.getName().equals("rename")) {
			
			if(!player.hasPermission("minez.addon1.rename")) {
				player.sendMessage(ChatColor.RED + "Couldn't change name: You don't have permission.");
				return false;
			}
			
			if(args.length==0) {
				player.sendMessage(ChatColor.RED + "Couldn't change name: You need to specify the name!");
				return false;
			}
			
			if(!player.getInventory().contains(Material.NAME_TAG)) {
				player.sendMessage(ChatColor.RED + "Couldn't change name: You need to have a name tag!");
				return false;
			}

			ItemStack item = player.getInventory().getItemInMainHand();
			ItemMeta meta = item.getItemMeta();
			
			if(meta.hasDisplayName()) {
				for(String s: plugin.getConfig().getStringList("rename.banned")) {
					if(meta.getDisplayName().contains(s)) {
						player.sendMessage(ChatColor.RED + "Couldn't change name: You can't rename that item!");
						return false;
					}
				}
			}
			
			if(!isAValidMaterial(item.getType())) {
				player.sendMessage(ChatColor.RED + "Couldn't change name: You can't rename this item!");
				return false;
			}
			
			String newName = "" + ChatColor.ITALIC;
			for(int i = 0; i < args.length; i++) {
				if(i == 0 ) {
					newName = newName + args[i];
					continue;
				}
				newName = newName + " " + args[i];		
			}
			
			for(String s: plugin.getConfig().getStringList("rename.banned")) {
				if(newName.contains(s)) {
					player.sendMessage(ChatColor.RED + "Couldn't change name: You can't use that name!");
					return false;
				}
			}
			
			if(player.hasPermission("minez.addon1.renameformat")) {
				newName = format(newName);
			}
			
			meta.setDisplayName(newName);
			
			item.setItemMeta(meta);
			player.getInventory().setItemInMainHand(item);
			removeNameTag(player.getInventory());
			return true;
		}
		return false;
	}
	

	private String format(String s) {
		if(s == null) return s;
		
		String result = s.replaceAll("&", "\u00a7");
		return result;
	}

	private void removeNameTag(PlayerInventory inventory) {
		for(int i = 0; i < inventory.getSize(); i++) {
			ItemStack item = inventory.getItem(i);
			if(item == null) continue;
			if(!item.getType().equals(Material.NAME_TAG)) continue;
			
			item.setAmount(item.getAmount()-1);
			inventory.setItem(i, item);
			return;
		}
	}

	private boolean isAValidMaterial(Material m) {
		if(plugin.getConfig().getBoolean("rename.all")) return true;
		
		if(plugin.getConfig().getBoolean("rename.armor")) {
			if(m.equals(Material.LEATHER_BOOTS)) return true;
			if(m.equals(Material.LEATHER_LEGGINGS)) return true;
			if(m.equals(Material.LEATHER_CHESTPLATE)) return true;
			if(m.equals(Material.LEATHER_HELMET)) return true;
			
			if(m.equals(Material.CHAINMAIL_BOOTS)) return true;
			if(m.equals(Material.CHAINMAIL_LEGGINGS)) return true;
			if(m.equals(Material.CHAINMAIL_CHESTPLATE)) return true;
			if(m.equals(Material.CHAINMAIL_HELMET)) return true;
			
			if(m.equals(Material.IRON_BOOTS)) return true;
			if(m.equals(Material.IRON_LEGGINGS)) return true;
			if(m.equals(Material.IRON_CHESTPLATE)) return true;
			if(m.equals(Material.IRON_HELMET)) return true;
		}
		
		if(plugin.getConfig().getBoolean("rename.tools")) {
			if(m.equals(Material.WOOD_SPADE)) return true;
			if(m.equals(Material.WOOD_HOE)) return true;
			if(m.equals(Material.WOOD_AXE)) return true;
			if(m.equals(Material.WOOD_PICKAXE)) return true;
			
			if(m.equals(Material.STONE_SPADE)) return true;
			if(m.equals(Material.STONE_HOE)) return true;
			if(m.equals(Material.STONE_AXE)) return true;
			if(m.equals(Material.STONE_PICKAXE)) return true;
			
			if(m.equals(Material.IRON_SPADE)) return true;
			if(m.equals(Material.IRON_HOE)) return true;
			if(m.equals(Material.IRON_AXE)) return true;
			if(m.equals(Material.IRON_PICKAXE)) return true;
			
			if(m.equals(Material.DIAMOND_SPADE)) return true;
			if(m.equals(Material.DIAMOND_HOE)) return true;
			if(m.equals(Material.DIAMOND_AXE)) return true;
			if(m.equals(Material.DIAMOND_PICKAXE)) return true;
			
			if(m.equals(Material.SHEARS)) return true;
			if(m.equals(Material.FISHING_ROD)) return true;
		}
		
		
		if(plugin.getConfig().getBoolean("rename.weapons")) {
			if(m.equals(Material.DIAMOND_SWORD)) return true;
			if(m.equals(Material.IRON_SWORD)) return true;
			if(m.equals(Material.STONE_SWORD)) return true;
			if(m.equals(Material.WOOD_SWORD)) return true;
			if(m.equals(Material.BOW)) return true;
		}
		
		return false;
	}
}
