package com.gmail.lJuanGBMinecraft;
import java.util.Arrays;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

import com.gmail.lJuanGBMinecraft.Fireworks.OpenFireworksMenuCommand;
import com.gmail.lJuanGBMinecraft.Fireworks.Listeners.ExplodeEventListener;
import com.gmail.lJuanGBMinecraft.Fireworks.Listeners.LaunchEventListener;
import com.gmail.lJuanGBMinecraft.Fireworks.Listeners.MenuClickListener;
import com.gmail.lJuanGBMinecraft.Fireworks.Listeners.MenuCloseListener;
import com.gmail.lJuanGBMinecraft.Fireworks.Listeners.OpenFireworksMenuListener;
import com.gmail.lJuanGBMinecraft.Fireworks.SpecialFireworks.SpecialFireworks;

public class AddonStartUp extends JavaPlugin{
	
	@Override
	public void onEnable() {	
		loadConfig();
		
		getCommand("eventitem").setExecutor(new EventItemCommand());
		getCommand("rename").setExecutor(new RenameCommand(this));
		
		Bukkit.getServer().getPluginManager().registerEvents(new ServerPotionListener(this), this);
		
		new SpecialFireworks();
		getCommand("firework").setExecutor(new OpenFireworksMenuCommand(this));
		Bukkit.getServer().getPluginManager().registerEvents(new OpenFireworksMenuListener(this), this);
		Bukkit.getServer().getPluginManager().registerEvents(new MenuClickListener(), this);
		Bukkit.getServer().getPluginManager().registerEvents(new MenuCloseListener(this), this);
		Bukkit.getServer().getPluginManager().registerEvents(new LaunchEventListener(this), this);
		Bukkit.getServer().getPluginManager().registerEvents(new ExplodeEventListener(this), this);
		
		//In rare case of reload: remove all metadata.
		for(Player p : this.getServer().getOnlinePlayers()) {
			if(p.hasMetadata("recentlyLaunchedFirework")) p.removeMetadata("recentlyLaunchedFirework", this);
			if(p.hasMetadata("openFireworkMenu")) {
				p.closeInventory();
				p.removeMetadata("openFireworkMenu", this);
			}
		}
	}
	
	@Override
	public void onDisable() {
	}
	
	
	private void loadConfig() {
		this.getConfig().addDefault("modifiedHealthPotions", true);
		
		this.getConfig().addDefault("fireworkdelay", 20*5);
		
		this.getConfig().addDefault("rename.weapons", true);
		this.getConfig().addDefault("rename.tools", true);
		this.getConfig().addDefault("rename.armor", true);
		this.getConfig().addDefault("rename.all", false);
		//Should include all legendaries name. There are other safety mechanisms.
		String[] bannedTerms = new String[] {"Flail", 
											"Truth Bow", 
											"Simoon's Song", 
											"Simoon's Melody", 
											"Simoon's Sonata", 
											"Simoon's Tune", 
											"Binding Bow", 
											"Overkill", 
											"Rocksteady", 
											"Vampyr", 
											"Therum's Strength", 
											"Therum's Power", 
											"Therum's Force", 
											"Therum's Might", 
											"Zombie Bow", 
											"Zombie head", 
											"Rabbit Feet", 
											"Venom Bow", 
											"Agni's Rage", 
											"Agni's Fury", 
											"Agni's Wrath", 
											"Agni's Ire", 
											"Corsair's Edge", 
											"Ipsey's Folly", 
											"Binding Helmet", 
											"Heal Bow", 
											"Robber's Blade", 
											"Spike Thrower", 
											"Hurter", 
											"Lone Sword", 
											"Meteor Rain", 
											"Shotbow", 
											"Ninja Sandals", 
											"Pluvia's Storm", 
											"Pluvia's Rain", 
											"Pluvia's Tempest", 
											"Pluvia's Hail", 
											"Gamble", 
											"Earth Shaker", 
											"Dentril's Fear", 
											"Dentril's Fright", 
											"Dentril's Terror", 
											"Dentril's Dread", 
											"Rubber Shield", 
											"Endeavour", 
											"Peace", 
											"Quiet", 
											"Kikuichimonji", 
											"Void Bow", 
											"Night's Shadow", 
											"Phantom Blade", 
											"Muramasa",
											"Gorgon's Gaze",
											"Grass Blade",
											"Mjolnir",
											"Spray Painter",
											"Spookyfest Pumpkin",
											"Tenseiga",
											"Wither Shot"};
		this.getConfig().addDefault("rename.banned", Arrays.asList(bannedTerms));
		
		this.getConfig().options().copyDefaults(true);
	    this.saveConfig();
	}
}
