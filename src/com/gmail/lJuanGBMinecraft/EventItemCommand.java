package com.gmail.lJuanGBMinecraft;

import java.time.LocalDate;
import java.time.format.TextStyle;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Locale;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

/**
 * Command to change the item in your hand to something customized. Allows to set custom name and lore with just a
 * command, instead of having to use /give. Also has options to include the giver and the date in the lore.
 * 
 * @author lJuanGB
 */
public class EventItemCommand implements CommandExecutor{
	
	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String commandName, String[] args) {
		if (!(sender instanceof Player)) return false;
		
		Player player = (Player) sender;

		if(cmd.getName().equals("eventitem")) {
			
			if(!player.hasPermission("minez.addon1.eventitem")) {
				return false;
			}
			
			ItemStack item = player.getInventory().getItemInMainHand();
			if(item == null || item.getType().equals(Material.AIR)) {
				player.sendMessage(ChatColor.RED+"You must hold an item in you main hand");
				return false;
			}
			
			//If the length of the command is less than 3, place defaut values in the method getItem
			ItemStack newItem;
			switch (args.length) {
			case 0: newItem = getItem(item, false, false, player.getName(), null, null); break;
			case 1: newItem = getItem(item, (args[0].equals("1")), false, player.getName(), null, null); break;
			case 2: newItem = getItem(item, (args[0].equals("1")), (args[1].equals("1")), player.getName(), null, null); break;
			case 3: newItem = getItem(item, (args[0].equals("1")), (args[1].equals("1")), player.getName(), args[2], null); break;
			default:
				//Obtain lore from the string of arguments
				String[] lore = Arrays.copyOfRange(args, 3, args.length);
				newItem =  getItem(item, (args[0].equals("1")), (args[1].equals("1")), player.getName(), args[2], lore); 
				break;
			}
			
			player.getInventory().setItemInMainHand(newItem);
		}
		return false;
	}
	
	
	public ItemStack getItem(ItemStack baseItem, boolean includeDate, boolean includeGiver, String giver, String name, String[] lore) {
		
		ItemStack item = baseItem.clone();
		ItemMeta meta = item.getItemMeta();
		
		
		if(name != null) {
			meta.setDisplayName(format(name));
		}
		
		
		ArrayList<String> newLore = new ArrayList<String>();
		if(meta.hasLore()) newLore = (ArrayList<String>) meta.getLore();
		
		
		if(includeGiver) {
			String whoAwarded = ChatColor.GRAY + "Awarded by: " + giver;
			newLore.add(0, whoAwarded);
		}
		
		
		if(includeDate) {
			LocalDate localDate = LocalDate.now();
			String whenObtained = ChatColor.GRAY + "Obtained: " + 
									localDate.getMonth().getDisplayName(TextStyle.SHORT, Locale.getDefault()).toUpperCase() + " " +
									localDate.getDayOfMonth() + ", " + 
									localDate.getYear();
			newLore.add(0, whenObtained);
		}
		
		
		if(lore != null) {
			if(includeDate || includeGiver) newLore.add("");
			
			for(int i = 0; i<=lore.length-1; i++) {
				newLore.add(ChatColor.GRAY + format(lore[i]));
			}
		}
		
		
		meta.setLore(newLore);
		item.setItemMeta(meta);
		
		return item;
	}

	
	private String format(String s) {
		if(s == null) return s;
		
		String result = s.replaceAll("&", "\u00a7");
		result = result.replaceAll("_", " ");
		return result;
	}
}